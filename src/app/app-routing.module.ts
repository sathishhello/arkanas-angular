import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'pages', pathMatch: 'full' },

    {
        path: 'pages',
        loadChildren: './pages/pages.module#PagesModule',
        // canActivate: [SeoGuard],
        data: {
            title: ['Pages'],
            desc: 'Welcome'
        }
    },
    //   {path:'not-found', component: NotFoundComponent},
    {
        path: '**', redirectTo: 'not-found'
    },

];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            useHash: true,
            enableTracing: false
        })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
