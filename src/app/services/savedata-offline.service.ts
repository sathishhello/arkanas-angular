import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';
import Dexie from 'dexie';
// import { Todo } from '../models/todo';
import { OnlineOfflineService } from './online-offline.service';
import { DataModel } from '../models/common-entity';

@Injectable({ providedIn: 'root' })
export class savedataOfflineService {
    private saveApi: DataModel[] = [];
    private db: any;
    constructor(private readonly onlineOfflineService: OnlineOfflineService) {
        this.registerToEvents(onlineOfflineService);
    
        this.createDatabase();
      }

      getStoreData() {
        return this.saveApi;
      }

      saveData(data: any) {
        data.id = UUID.UUID();
        this.saveApi.push(data);
    
        // if (!this.onlineOfflineService.isOnline) {
          this.addToIndexedDb(data);
        // }
      }

      private addToIndexedDb(data: DataModel) {
        this.db.localdata
          .add(data)
          .then(async () => {
            const allItems: DataModel[] = await this.db.localdata.toArray();
            console.log('saved in DB, DB is now', allItems);
          })
          .catch(e => {
            alert('Error: ' + (e.stack || e));
          });
      }

      private registerToEvents(onlineOfflineService: OnlineOfflineService) {
        onlineOfflineService.connectionChanged.subscribe(online => {
          if (online) {
            console.log('went online');
            console.log('sending all stored items');
            this.sendItemsFromIndexedDb();
          } else {
            console.log('went offline, storing in indexdb');
          }
        });
      }

      private async sendItemsFromIndexedDb() {
        const allItems: DataModel[] = await this.db.localdata.toArray();
        allItems.forEach((item: DataModel) => {
          this.db.localdata.delete(item.id).then(() => {
            console.log(`item ${item.id} sent and deleted locally`);
          });
        });
      }

      private createDatabase() {
        this.db = new Dexie('Cjams');
        this.db.version(1).stores({
          localdata: 'id,value,done'
        });
      }
}