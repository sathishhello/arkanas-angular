export class DataModel {
    id: any;
    apiName: '';
    methodName: string;
    request: any;
    response: any;
    syncStatus: boolean;
}