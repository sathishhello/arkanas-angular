import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { HomeDashboardComponent } from './home-dashboard/home-dashboard.component';
import { NarrativeComponent } from './narrative/narrative.component';
import { PersonComponent } from './person/person.component';

const routes: Routes = [
  {
    path:'',
    component: PagesComponent,
    children: [
      {
        path:'dashboard',
        component: HomeDashboardComponent
      },
      {
        path:'narrative',
        component: NarrativeComponent
      },
      {
        path:'person',
        component: PersonComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
