import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { HomeDashboardComponent } from './home-dashboard/home-dashboard.component';
import { NarrativeComponent } from './narrative/narrative.component';
import { PersonComponent } from './person/person.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule
  ],
  declarations: [PagesComponent, HomeDashboardComponent, NarrativeComponent, PersonComponent, HeaderComponent]
})
export class PagesModule { }
