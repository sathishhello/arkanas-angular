import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { TodoService } from 'src/app/services/todo.service';
import { savedataOfflineService } from 'src/app/services/savedata-offline.service';
import { OnlineOfflineService } from 'src/app/services/online-offline.service';

@Component({
  selector: 'app-home-dashboard',
  templateUrl: './home-dashboard.component.html',
  styleUrls: ['./home-dashboard.component.css']
})
export class HomeDashboardComponent implements OnInit {
  caseList: any;
  caseData: any;
  ir: any;
  allData: any;
  constructor(private http: HttpClient, private saveOffline: savedataOfflineService, private checkOnline: OnlineOfflineService) { }

  ngOnInit() {
    this.allData = this.saveOffline.getStoreData();
    this.caseData = this.allData.find((i)=> i.apiName === 'home');
    if (this.checkOnline.isOnline) {
      console.log('is online');
      this.caseInfo();
    } else {
      console.log('status offlie');
      const getHomePage = this.allData.find((i)=> i.apiName === 'home');
      this.caseList = getHomePage.response;
    }
    console.log(this.caseList)
    // this.caseList =  this.allData[0].response;
    this.getIR();
  }

  getIR() {
    this.http.get('https://api-re-cw.cardinalityai.xyz/api/servicerequestsearches/usersservicerequest?data={%22count%22:-1,%22where%22:{%22actiontype%22:%22AR%22,%22status%22:%22inprogress%22,%22sort%22:{%22active%22:%22approvaldate%22,%22direction%22:%22desc%22}},%22page%22:1,%22limit%22:10,%22method%22:%22get%22}').subscribe((res)=> {
      console.log(res);
    })
  }
  caseInfo() {
    this.http.get('https://api-cs-base.cardinalityai.xyz/cases/list').subscribe((res) => {
      this.caseList = res;
      const saveData = {
        apiName: 'home',
        method: 'get',
        request: 'any',
        response: res,
        syncStatus: false
      }
      if(!this.caseData) {
        this.saveOffline.saveData(saveData)
      }
    },
      (err) => {
        console.log(err)
      })
  }

  saveOfflineData() {
    this.ir = { "data": [{ "groupnumber": "", "intakeserviceid": "b8c0006d-6bfe-499a-be90-ce520eb12d5c", "legalguardian": "Erica   Hill ", "servicerequestnumber": "20200232016430", "srtype": "Child Protective Services", "srsubtype": "CPS-IR", "datereceived": "2020-08-19T11:18:14.879Z", "displayname": "", "county": "", "duedate": null, "status": "Assigned", "accepteddate": "2020-08-19T12:40:12.822Z", "routedon": "2020-08-19T12:11:16.594Z", "ismaltreatment": 0, "approvaldate": "2020-08-19T12:11:01.797Z", "foldertype": "", "isfatality": 0, "restrictstatus": "INCL", "focusname": "Morgan  Hill", "fatalityinfo": null, "open_closed": "In Progress", "intakedaterecieved": "2020-08-19T12:10:53.615Z", "iscovid19": null, "covid19_diagnosis": null, "iconurl": null }, { "groupnumber": "", "intakeserviceid": "e436984c-e154-465b-8915-ce63c1946a4a", "legalguardian": null, "servicerequestnumber": "20200232016429", "srtype": "Child Protective Services", "srsubtype": "CPS-IR", "datereceived": "2020-08-19T07:05:06.983Z", "displayname": "", "county": "", "duedate": null, "status": "Assigned", "accepteddate": "2020-08-19T11:15:46.028Z", "routedon": "2020-08-19T09:45:47.062Z", "ismaltreatment": 0, "approvaldate": "2020-08-19T09:45:10.611Z", "foldertype": "", "isfatality": 0, "restrictstatus": "INCL", "focusname": "Wilma  Fortner I", "fatalityinfo": null, "open_closed": "In Progress", "intakedaterecieved": "2020-08-19T09:44:46.145Z", "iscovid19": null, "covid19_diagnosis": null, "iconurl": null }, { "groupnumber": "", "intakeserviceid": "c79a65cc-79fb-482c-a687-a3df65e2558a", "legalguardian": null, "servicerequestnumber": "20200231016428", "srtype": "Child Protective Services", "srsubtype": "CPS-IR", "datereceived": "2020-08-18T12:05:51.359Z", "displayname": "", "county": "", "duedate": null, "status": "Assigned", "accepteddate": "2020-08-19T07:13:44.242Z", "routedon": "2020-08-18T13:53:06.488Z", "ismaltreatment": 0, "approvaldate": "2020-08-18T13:52:39.532Z", "foldertype": "", "isfatality": 0, "restrictstatus": "INCL", "focusname": "Wilma   Fortner ", "fatalityinfo": null, "open_closed": "In Progress", "intakedaterecieved": "2020-08-18T17:32:36.000Z", "iscovid19": null, "covid19_diagnosis": null, "iconurl": null }, { "groupnumber": "", "intakeserviceid": "a8318b95-6cc7-4d84-8f02-b9f1854b3f69", "legalguardian": "Jane   Geything ", "servicerequestnumber": "20200231016427", "srtype": "Child Protective Services", "srsubtype": "CPS-IR", "datereceived": "2020-08-18T12:16:10.427Z", "displayname": "", "county": "", "duedate": null, "status": "Assigned", "accepteddate": "2020-08-19T07:05:03.494Z", "routedon": "2020-08-18T12:31:40.009Z", "ismaltreatment": 0, "approvaldate": "2020-08-18T12:31:35.063Z", "foldertype": "", "isfatality": 0, "restrictstatus": "INCL", "focusname": "Wilma  Fortner I", "fatalityinfo": null, "open_closed": "In Progress", "intakedaterecieved": "2020-08-18T17:42:00.000Z", "iscovid19": null, "covid19_diagnosis": null, "iconurl": null }, { "groupnumber": "", "intakeserviceid": "e4ff78bf-e9a3-49da-aff7-35002bbb6232", "legalguardian": "ANTHONY M AIREY", "servicerequestnumber": "20200231016426", "srtype": "Child Protective Services", "srsubtype": "CPS-IR", "datereceived": "2020-08-18T11:44:36.300Z", "displayname": "", "county": "", "duedate": null, "status": "Assigned", "accepteddate": "2020-08-18T13:38:20.439Z", "routedon": "2020-08-18T12:03:58.725Z", "ismaltreatment": 0, "approvaldate": "2020-08-18T12:03:47.455Z", "foldertype": "", "isfatality": 0, "restrictstatus": "INCL", "focusname": null, "fatalityinfo": null, "open_closed": "In Progress", "intakedaterecieved": "2020-08-18T17:14:11.000Z", "iscovid19": null, "covid19_diagnosis": null, "iconurl": null }, { "groupnumber": "", "intakeserviceid": "81b54741-73f6-4246-82cd-6e9b84edcfc1", "legalguardian": "SHAMBEL G ALEMU", "servicerequestnumber": "20200220016420", "srtype": "Child Protective Services", "srsubtype": "CPS-IR", "datereceived": "2020-08-07T03:52:24.251Z", "displayname": "", "county": "", "duedate": null, "status": "Assigned", "accepteddate": "2020-08-18T11:26:03.011Z", "routedon": "2020-08-07T04:17:30.519Z", "ismaltreatment": 0, "approvaldate": "2020-08-07T04:16:46.854Z", "foldertype": "", "isfatality": 0, "restrictstatus": "INCL", "focusname": "Sam Bricks RAMAJ ADDISON", "fatalityinfo": null, "open_closed": "In Progress", "intakedaterecieved": "2020-08-07T09:21:47.000Z", "iscovid19": null, "covid19_diagnosis": null, "iconurl": null }, { "groupnumber": "", "intakeserviceid": "3d9bd455-080e-481f-8a02-cfac77eb3ca0", "legalguardian": "DUSTIN K BANE", "servicerequestnumber": "20200219016419", "srtype": "Child Protective Services", "srsubtype": "CPS-IR", "datereceived": "2020-08-06T13:55:18.323Z", "displayname": "", "county": "", "duedate": null, "status": "Assigned", "accepteddate": "2020-08-19T10:04:03.769Z", "routedon": "2020-08-06T15:45:25.120Z", "ismaltreatment": 0, "approvaldate": "2020-08-06T15:45:00.148Z", "foldertype": "", "isfatality": 0, "restrictstatus": "INCL", "focusname": "JUSTIN A AYALA PINEDA ", "fatalityinfo": null, "open_closed": "In Progress", "intakedaterecieved": "2020-08-06T19:24:18.000Z", "iscovid19": null, "covid19_diagnosis": null, "iconurl": null }, { "groupnumber": "", "intakeserviceid": "92ca6c69-afe0-4239-91e9-24edb4d39e8f", "legalguardian": "TAZEEN ASIYA AHMAD", "servicerequestnumber": "20200219016418", "srtype": "Child Protective Services", "srsubtype": "CPS-IR", "datereceived": "2020-08-06T07:41:51.259Z", "displayname": "", "county": "", "duedate": null, "status": "Assigned", "accepteddate": "2020-08-19T07:40:26.589Z", "routedon": "2020-08-06T08:07:13.645Z", "ismaltreatment": 0, "approvaldate": "2020-08-06T08:05:03.755Z", "foldertype": "", "isfatality": 0, "restrictstatus": "INCL", "focusname": null, "fatalityinfo": null, "open_closed": "In Progress", "intakedaterecieved": "2020-08-06T13:09:59.000Z", "iscovid19": null, "covid19_diagnosis": null, "iconurl": null }, { "groupnumber": "", "intakeserviceid": "f6d2012b-da53-4e7c-88c4-fb956b467f7b", "legalguardian": null, "servicerequestnumber": "20200217016416", "srtype": "Child Protective Services", "srsubtype": "CPS-IR", "datereceived": "2020-08-04T07:38:02.740Z", "displayname": "", "county": "", "duedate": null, "status": "Assigned", "accepteddate": "2020-08-19T08:53:36.420Z", "routedon": "2020-08-04T07:42:00.159Z", "ismaltreatment": 0, "approvaldate": "2020-08-04T07:41:36.229Z", "foldertype": "", "isfatality": 0, "restrictstatus": "INCL", "focusname": "JADA  ADSIDE", "fatalityinfo": null, "open_closed": "In Progress", "intakedaterecieved": "2020-08-04T13:02:25.000Z", "iscovid19": null, "covid19_diagnosis": null, "iconurl": null }, { "groupnumber": "", "intakeserviceid": "6e45c8a0-19d9-4c63-93ad-a730ec9bcc62", "legalguardian": "Kevin  Taylor ", "servicerequestnumber": "20200216016413", "srtype": "Child Protective Services", "srsubtype": "CPS-IR", "datereceived": "2020-07-03T03:26:34.815Z", "displayname": "", "county": "", "duedate": null, "status": "Assigned", "accepteddate": "2020-08-03T09:34:23.170Z", "routedon": "2020-08-03T04:24:56.285Z", "ismaltreatment": 0, "approvaldate": "2020-08-03T04:24:30.542Z", "foldertype": "", "isfatality": 0, "restrictstatus": "INCL", "focusname": "Alexander  Taylor ", "fatalityinfo": null, "open_closed": "In Progress", "intakedaterecieved": "2020-07-03T08:55:33.000Z", "iscovid19": null, "covid19_diagnosis": null, "iconurl": null }], "count": "18" }
    const saveData = {
      apiName: '',
      methodName: 'get',
      request: 'any',
      response: this.ir,
      syncStatus: false
    }
    this.saveOffline.saveData(saveData)

  }

}
